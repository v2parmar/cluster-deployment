Useful commands for building and establishing the RabbitMQ service (NOTE: must be executed from the root directory of rabbitmq):
1. Build a new docker image (make sure to delete old images created using this command before creating a new one)
	docker build -t rabbitmq:0.1 . -f Dockerfile
	
2. Use Helm to install the statefulset, pods, services, configmaps, etc. to the current kubernetes cluster
	- NOTE: make sure you are aware of what context/cluster you are on (local, dev, staging)
	- NOTE: make sure to delete the previous helm release before running this command (i.e. use 'helm ls' to list all releases and 'helm delete <release_name>' to delete a specific one)
	helm install .

3. To access RabbitMQ management UI:
	- NOTE: enter this command in a command prompt terminal and then open a browser to http://localhost:15672
	kubectl port-forward --namespace rabbit svc/rabbitmq 15672:15672

	
Some notes on this RabbitMQ configuration:
- any changes to files in the "conf" folder will require a re-build of the docker image (see command 1 above)